# 
from machine import Pin, disable_irq, enable_irq
from time import sleep
from utime import ticks_ms


led = Pin(19, Pin.OUT)
botao = Pin(34, Pin.IN)

botao_pressionado = False
ultimo_tick = 0

def handle_pin(pin):
    global ultimo_tick, botao_pressionado
    agora = ticks_ms()
    print(agora- ultimo_tick)
    if (agora - ultimo_tick) > 300:
        botao_pressionado = True
        ultimo_tick = agora    

    
led.off()

botao.irq(trigger=Pin.IRQ_RISING, handler=handle_pin)

while True:
    if botao_pressionado:
        led.value(not led.value())
        botao_pressionado = False
        

        
