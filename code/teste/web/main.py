# 
from machine import Pin
from time import sleep

led1 = Pin(21, Pin.OUT)
led2 = Pin(33, Pin.OUT)
delay = 0.2 # Ajusta o intervalo entre cada piscada

# Repita para sempre
while True:
    led1.on()
    sleep(delay)
    
    led2.on()
    led1.off()
    sleep(delay)
    led2.off()
