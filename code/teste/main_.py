from machine import Pin, PWM, ADC
from time import sleep
from math import sin

frequency = 5000
led = PWM(Pin(15), frequency)
w = 10
t = 0

mic = ADC(Pin(14, Pin.IN))
mic.atten(ADC.ATTN_11DB)
mic.width(ADC.WIDTH_12BIT)

while True:
    duty_cycle = 500*sin(w*t) + 500
    #sleep(0.001)
    #t += 0.005
    led.duty(int(duty_cycle))
    
    som = mic.read()
    sons = 0
    for i in range(10):
        sons += som
    print(sons/10)
    
    #sleep(0.1)
     
                 