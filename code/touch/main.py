from machine import TouchPad, Pin
from time import sleep

# 27, 33, 32
touch_pin = TouchPad(Pin(27))

while True:
    touch_value = touch_pin.read()
    print(touch_value)
    sleep(0.5)
