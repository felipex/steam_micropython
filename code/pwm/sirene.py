from machine import Pin, PWM
from time import sleep
from math import sin

frequency = 5000
led = PWM(Pin(15), frequency)
w = 0.1
t = 0
while True:
    duty_cycle = 500*sin(w*t) + 500
    #sleep(0.001)
    t += 0.005
    led.duty(int(duty_cycle))