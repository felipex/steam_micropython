from machine import Pin, PWM
import time
tempo = 5
tones = {
    'c': 262,
    'd': 294,
    'e': 330,
    'f': 349,
    'g': 392,
    'a': 440,
    'b': 494,
    'C': 523,
    ' ': 0,
}
beeper = PWM(Pin(15, Pin.OUT), freq=440, duty=512)
melody = 'cdefgabC'
rhythm = [8, 8, 8, 8, 8, 8, 8, 8]

melody2 = 'abfgadfge gcfegcfed'
rhythm2 = [500,300,250,250,250,300,200,200,700,200,500,500,200,200,200,500,200,200,500]

for tone, length in zip(melody2, rhythm2):
    beeper.duty(512)
    if tones[tone] > 0:
        beeper.freq(tones[tone])
    else:
        beeper.duty(0)
        
    #time.sleep(tempo/length)
    time.sleep(length/1000)
    
beeper.deinit()