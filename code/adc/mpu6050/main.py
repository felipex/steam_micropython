from machine import SoftI2C
from machine import Pin
from machine import sleep
import mpu6050

i2c = SoftI2C(scl=Pin(22), sda=Pin(21))     #initializing the I2C method for ESP32

print('Scan i2c bus...')

devices = i2c.scan()
if len(devices) == 0:
    print("No i2c device !")
else:
    print('i2c devices found:',len(devices))
  
for device in devices:  
    print("Decimal address: ",device," | Hexa address: ",hex(device))

mpu= mpu6050.accel(i2c)
  
while True:
    mpu.get_values()
    #print(mpu.get_values())
    acc = mpu.get_values()
    print(acc['AcX'], acc['AcY'], acc['AcZ'])
    sleep(500)
