from machine import Pin, ADC
from time import sleep

# ADC1 pins 32,33,34,35,36,39
# ADC2 pins 0,2,4,12,13,14,15,25,26,27

potenciometro = ADC(Pin(35))
potenciometro.atten(ADC.ATTN_11DB) # de 0 a 3.3V
# ADC.ATTN_0DB — the full range voltage: 1.2V
# ADC.ATTN_2_5DB — the full range voltage: 1.5V
# ADC.ATTN_6DB — the full range voltage: 2.0V
# ADC.ATTN_11DB — the full range voltage: 3.3V  - Default

potenciometro.width(ADC.WIDTH_10BIT)
# ADC.WIDTH_9BIT: range 0 to 511
# ADC.WIDTH_10BIT: range 0 to 1023
# ADC.WIDTH_11BIT: range 0 to 2047
# ADC.WIDTH_12BIT: range 0 to 4095 - Default

while True:
    print(potenciometro.read())
    sleep(0.5)



