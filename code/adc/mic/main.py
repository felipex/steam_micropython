from machine import Pin, ADC
from time import sleep, time

mic = ADC(Pin(14, Pin.IN))
mic.atten(ADC.ATTN_11DB)
mic.width(ADC.WIDTH_12BIT)

while True:
    som = mic.read()
    sons = 0
    for i in range(10):
        sons += som
        
    print(sons/10)
    
    #sleep(0.1)
     