from machine import Pin, ADC
from time import sleep


potenciometro = ADC(Pin(35))
potenciometro.atten(ADC.ATTN_11DB) 
potenciometro.width(ADC.WIDTH_10BIT)

led1 = Pin(16, Pin.OUT)
led2 = Pin(17, Pin.OUT)
led3 = Pin(18, Pin.OUT)
led4 = Pin(19, Pin.OUT)

while True:
    intensidade = int(potenciometro.read() / 205)
    print(intensidade)
    led1.off()
    led2.off()
    led3.off()
    led4.off()
    
    if intensidade == 1:
        led1.on()
    if intensidade == 2:
        led1.on()
        led2.on()
    if intensidade == 3:
        led1.on()
        led2.on()
        led3.on()
    if intensidade == 4:
        led1.on()
        led2.on()
        led3.on()
        led4.on()
    
    sleep(0.5)



