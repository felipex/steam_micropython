from machine import Pin, ADC
from time import sleep, time

ldr = ADC(Pin(36, Pin.IN))
ldr.atten(ADC.ATTN_11DB)
ldr.width(ADC.WIDTH_12BIT)

while True:
    claridade = ldr.read() 
    print(claridade)
    sleep(0.1)
     