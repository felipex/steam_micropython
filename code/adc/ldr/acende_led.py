from machine import Pin, ADC
from time import sleep, time

ldr = ADC(Pin(14, Pin.OUT))
ldr.atten(ADC.ATTN_11DB)
ldr.width(ADC.WIDTH_12BIT)

led = Pin(16, Pin.OUT)

while True:
    claridade = ldr.read() 
    print(time(), claridade)
    if (claridade < 150):
        led.on()
    else:
        led.off()
        
    sleep(0.3)
    