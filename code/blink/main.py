#

##### Seção: CONFIGURAÇÃO INICIAL ######
# Esta seção será executado somente na (re)inicialização do microcontrolador.

from machine import Pin
from time import sleep

# Informa que o pino 16 será usado como saída.
led = Pin(2, Pin.OUT)  # LED interno
#led = Pin(16, Pin.OUT)  # LED externo

##### FIM CONFIGURAÇÃO INICIAL ######

##### Seção: LOOP INFINITO ######
# Tudo o que está dentro deste laço será executado enquanto o microcontrolador estiver ligado.
while True:
    led.on()  # Liga o LED
    sleep(1)  # Espera 1 segundo. Tente mudar esse valor para 0.3 ou 2 e veja o que acontece.
    led.off()
    sleep(1)
