# 
from machine import Pin
from time import sleep

led1 = Pin(16, Pin.OUT)
led2 = Pin(17, Pin.OUT)
led3 = Pin(18, Pin.OUT)
led4 = Pin(19, Pin.OUT)

delay = 0.2 # Ajusta o intervalo entre cada piscada

# Repita para sempre
while True:
    # Pisca 1
    led1.on()
    sleep(delay)
    
    # Pisca 2
    led2.on()
    led1.off()
    sleep(delay)
    
    # Pisca 3
    led3.on()
    led2.off()
    sleep(delay)
    
    # Pisca 4
    led4.on()
    led3.off()
    sleep(delay)

    # Apaga pra começar tudo de novo
    led4.off()
