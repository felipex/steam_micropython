
### Exercícios
* Vamos facilitar as coisas, use *led.toggle()* em vez de *led.sleep()* .
* Tente mudar a frequência com que o LED pisca para entender o funcionamento.
* Tente dimuinir a frequência e observe o que acontece. Será que deu defeito?  
* Use dois LEDs e faça-os pisca alternadamente.
* Use os quatro LEDs para fazer eles piscarem em sequência.
  (Os pinos dos LEDs são 16,17,18,19.
* Aproveite que já está craque no blink e brinque a vontade explorando o que pode ser feito.

### Desafios
* Faça um semáforo piscando na ordem certa. Tente usar intervalos que pareçam reais.
* Faça dois semáforos trabalhando em conjunto como se estivesse em um cruzamento. 
  (Aqui já pode ser interessante utilizar a abordagem de estados finitos)
  
  
### Referência
* https://www.coderdojotc.org/micropython/basics/03-blink/