## O que é Computação Física?

Physical Computing is the process of using computers to read data from sensors about the world around us and then taking actions on this incoming data stream. These actions are typically doing things like blinking and LED, moving a motor or updating a display.

Computação Física é o processo de usar computadores para ler dados de sensores.

