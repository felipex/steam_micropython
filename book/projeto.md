**Observação** - Tem muita coisa anotada aqui (https://codeberg.org/felipex/Estudos_e_Projetos/src/branch/main/upython_para_crian%C3%A7as.md)

Bibliotecas micropython - https://github.com/mcauser/awesome-micropython

#### Teoria
   * **Output digital** - Circuito simples com LED e com buzzer
      - Comando de saĩda 
   * **Input digital** - Push button e chave
      - Funcionamento de um evento de entrada
   * **Temporização** - Blink
      - Funcionamento do evento de tempo 
   * **Input analógico** - Potenciômetro, LDR, Sensor de proximidade, sensor de temperatura
   * **Output analógico** - LED Fade, motor
   * **Misturando tudo** - Potenciômetro alterando o brilho do led, potenciômetro alterando a frequência de pisca 
   
   * **Sensores invisíveis** - Infravermelho
   
   **MATERIAL**
   * Circuito simples (Rutekit ou papel) e brinquedinho que apita
      - Bateria e LED/Buzzer
      - Bateria, botão e LED 
      
   * Kit genérico ESP32 com
      - LEDs, PushButton, LED RGB, Buzzer, Potenciômetro, LDR
      O kit básico deve conter componentes suficente para organizar algumas experiências iniciais: 
      - Semáforo
      - Acende na escuridão
      - Simon (também com touth)
      - Musical com teclado  (ou touch)
      - Ver que bate primeiro para jogos
      
      
   * Kits específicos
      - Alarme com laser, LDR, sirene e pisca
      - Semáforo (um e dois?)
      - Sensor de movimento/presença abrir porta
      - Tecladinho musical (também com músicas pré-definidas em arquivo txt)
      - Acender a luz quando escurecer
      - Gritômetro
      - Código Morse com luz/laser e ldr
      - Sensor magnético para porta
      - Jogo Genius/Simon
      - Senha para abrir porta (ou ligar qualquer coisa)
      - Acelerômetro como controle de algum dispositivo
         - Mostra valores na Tela
         - Acende LED de acordo com a inclinação
         - Faz som de acordo com a inclinação

      - Sensor de estacionamento
      - Ver que bate primeiro para jogos
      - (**Web**) Controle via web (webapp e app mobile)
      - (**Web**) Web como monitor
      - Ligar alguma coisa com infravermelho quando se aproximar
      - Alguma coisa com LED RGB

